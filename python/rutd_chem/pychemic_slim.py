import numpy as np
import os
import warnings
import matplotlib.pyplot as plt
import network_editor as ned

class ChemModl:
    """
    Class containing the chemical model data, parameters and meta-data.

    Define the network structure, we need reactants, products, 3 coefficients
    and some space for potential comments.
    """
    name = ''
    id  = None
    nt   = 0
    time = np.nan
    abun = np.nan
    ntw = network()
    
    hash = 0l
    
    def __str__():
        return self.name
    
    def __init__(self, pdrates=True, **kwargs):
        # Physical conditions as function of time
        self.x = kwargs.get('x',None)
        self.y = kwargs.get('y',None)
        self.z = kwargs.get('z',None)
        self.time = kwargs.get('time',None)
        self.nt = np.size(self.time)
        self.rho = kwargs.get('rho',None)
        self.Tg = kwargs.get('Tg',None)
        self.Td = kwargs.get('Td',None)
        self.G0 = kwargs.get('G0',None)
        self.AvIS = kwargs.get('AvIS',None)
        self.AvSt = kwargs.get('AvSt',None)
        self.AvArr = kwargs.get('AvArr',None)
        self.fH2_IS = kwargs.get('fH2_IS',None)
        self.fH2_St = kwargs.get('fH2_St',None)
        self.fCO_IS = kwargs.get('fCO_IS',None)
        self.fCO_St = kwargs.get('fCO_St',None)
        self.ZetaCR = kwargs.get('ZetaCR',None)
        self.ZetaX = kwargs.get('ZetaX',None)
        self.gdens = kwargs.get('gdens',None)
        self.amu = kwargs.get('amu',None)
        self.H = kwargs.get('H',None)  # Smoothing length
        self.AvLocal = kwargs.get('AvLocal',None)
        #
        # Abundances as function of time for each species
        spec = kwargs.get('spec',None)
        if spec != None:
            self.ns = np.size(spec)
            self.spec = spec
            self.sindex = np.arange(self.ns)
        else:
            self.ns = None
            self.spec = None
            self.sindex = None
        self.abuns = kwargs.get('abuns',None)
        #
        # Reactions and their rates as function of time
        self.InitSpec = kwargs.get('InitSpec',None)
        self.InitAbuns = kwargs.get('InitAbuns',None)
        self.r1 = kwargs.get('r1',None)
        self.r2 = kwargs.get('r2',None)
        self.p1 = kwargs.get('p1',None)
        self.p2 = kwargs.get('p2',None)
        self.p3 = kwargs.get('p3',None)
        self.p4 = kwargs.get('p4',None)
        self.p5 = kwargs.get('p5',None)
        if self.r1 != None:
            self.nre = np.size(self.r1)
            self.rindex = np.arange(nre)
        else:
            self.nre = None
            self.rindex = None
        self.ak = kwargs.get('ak',None)
        #
        # Metadata
        self.specfile = kwargs.get('specfile',None)
        self.ratefile = kwargs.get('ratefile',None)
        self.ID = kwargs.get('ID',None)
        self.rhod = kwargs.get('rhod',None)
        self.mdmg = kwargs.get('mdmg',None)
        self.albedo_UV = kwargs.get('albedo_UV',None)
        #
        # Store the production and destruction rates for all species:
        if (self.nre > 0 and pdrates):
            self.pdrates = []
            for cs in self.spec:
                self.pdrates.append(ReactionRates(self, cs))
        else:
            self.pdrates = None

    def __len__(self):
        '''
        Returns the number of timestep in the ChemModl object.
        '''
        return self.nt
    
    def __repr__():
        '''
        Returns model summary (ID, time steps, species, reactions).
        '''
        print 'ID {}: {} time steps, {} species and {} reactions'.format(self.ID, self.nt, self.ns, self.nr)
